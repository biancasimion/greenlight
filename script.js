(function(){
   var app = angular.module('greenlightViewer', ["ui.router"]);
   app.config(function($stateProvider, $urlRouterProvider){

      $urlRouterProvider.otherwise("/home")

      $stateProvider

          .state('home', {
             url:"/home",
             templateUrl: "home.html"
          })
          .state('about', {
             url:"/about",
             templateUrl: "about.html"
          })
          .state('account', {
             url:"/account",
             templateUrl: "account.html"
          })
          .state('notifications', {
             url:"/notifications",
             templateUrl: "notifications.html"
          })

   });

   var MainController = function($scope, $http, $location) {

      $scope.changeView = function (view){
         $location.path();
      }
      
      $scope.title = '$220';
      $scope.description = 'Bacon ipsum dolor amet dolore cupim enim id consequat, tri-tip magna t-bone beef tempor. Tri-tip tenderloin corned beef pork chop pig, porchetta nulla. Short ribs spare ribs aliqua biltong, labore ut doner dolore in. Venison qui dolor, pork ut nulla aute pork chop spare ribs tri-tip brisket in.';
   }
   
   app.controller('MainController', ["$scope", "$http", "$location", MainController]);
}());
