 //var locations = [
 //        ['Bondi Beach', -33.890542, 151.274856, 4],
 //        ['Coogee Beach', -33.923036, 151.259052, 5],
 //        ['Cronulla Beach', -34.028249, 151.157507, 3],
 //        ['Manly Beach', -33.80010128657071, 151.28747820854187, 2],
 //        ['Maroubra Beach', -33.950198, 151.259302, 1]
 //     ];
 //     var map;
 //     var markers = [];
 //
 //     function init() {
 //        map = new google.maps.Map(document.getElementById('map_canvas'), {
 //           zoom: 10,
 //           center: new google.maps.LatLng(-33.92, 151.25),
 //           mapTypeId: google.maps.MapTypeId.ROADMAP
 //        });
 //
 //        var num_markers = locations.length;
 //        for (var i = 0; i < num_markers; i++) {
 //           markers[i] = new google.maps.Marker({
 //              position: {
 //                 lat: locations[i][1],
 //                 lng: locations[i][2]
 //              },
 //              map: map,
 //              html: locations[i][0],
 //              id: i,
 //           });
 //
 //           google.maps.event.addListener(markers[i], 'click', function() {
 //              var infowindow = new google.maps.InfoWindow({
 //                 id: this.id,
 //                 content: this.html,
 //                 position: this.getPosition()
 //              });
 //              google.maps.event.addListenerOnce(infowindow, 'closeclick', function() {
 //                 markers[this.id].setVisible(true);
 //              });
 //              this.setVisible(false);
 //              infowindow.open(map);
 //           });
 //        }
 //     }
 //
 //     google.maps.event.addDomListener(window, 'load', init);
 jQuery(function($) {
     // Asynchronously Load the map API
     var script = document.createElement('script');
     script.src = "http://maps.googleapis.com/maps/api/js?sensor=false&callback=initialize";
     document.body.appendChild(script);
 });

 function initialize() {
     var map;
     var bounds = new google.maps.LatLngBounds();
     var mapOptions = {
         mapTypeId: 'roadmap'
     };

     // Display a map on the page
     map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
     map.setTilt(45);

     // Multiple Markers
     var markers = [
         ['London Eye, London', 51.503454,-0.119562],
         ['Palace of Westminster, London', 47.0000, 2.0000 ]
     ];

     // Info Window Content
     var infoWindowContent = [
         ['<div class="info_content">' +
         '<h3>$456</h3>' +
         '<p></p>' +        '</div>'],
         ['<div class="info_content">' +
         '<h3>$783</h3>' +
         '<p></p>' +
         '</div>']
     ];

     // Display multiple markers on a map
     var infoWindow = new google.maps.InfoWindow(), marker, i;

     // Loop through our array of markers & place each one on the map
     for( i = 0; i < markers.length; i++ ) {
         var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
         bounds.extend(position);
         marker = new google.maps.Marker({
             position: position,
             map: map,
             title: markers[i][0]
         });

          //Allow each marker to have an info window
         google.maps.event.addListener(marker, 'click', (function(marker, i) {
             return function() {
                 infoWindow.setContent(infoWindowContent[i][0]);
                 infoWindow.open(map, marker);
             }
         })(marker, i));

         // Automatically center the map fitting all markers on the screen
         map.fitBounds(bounds);
     }
     for(i = 0; i < markers.length; i++ ) {
         (function (marker, i) {

             infoWindow.setContent(infoWindowContent[i][0]);
             infoWindow.open(map, marker);
         }(marker, i));

     }


     // Override our map zoom level once our fitBounds function runs (Make sure it only runs once)
     var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
         this.setZoom(6);
         google.maps.event.removeListener(boundsListener);
     });

 }