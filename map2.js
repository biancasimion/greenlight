 var map;
 var priceBox = $('#priceBox');
 var markers = [];
 var locations = [
    ['£200', 47.0000, 2.0000, 4],
    ['£1230', 51.503454,-0.119562, 5]
    //['£234', -34.028249, 151.157507, 3],
    //['£123', -33.80010128657071, 151.28747820854187, 2],
    //['£768', -33.950198, 151.259302, 1]
 ];
 var uk = {
    lat: 51.5000,
    lng: 0.1167
 };

 function initMap() {

    map =  new google.maps.Map(document.getElementById('map_canvas'), {
       center: new google.maps.LatLng( 47.0000, 2.0000),
       // center: uk,
       mapTypeId: google.maps.MapTypeId.ROADMAP,
       zoom: 6
    });


    var num_markers = locations.length;
    for (var i = 0; i < num_markers; i++) {
       markers[i] = new google.maps.Marker({
          position: {
             lat: locations[i][1],
             lng: locations[i][2]
          },
          map: map,
          html: locations[i][0],
          id: i
       });

        var infowindow;
       google.maps.event.addListener(markers[i], 'click', function() {
           infowindow = new google.maps.InfoWindow({
             id: this.id,
             content: this.html,
             position: this.getPosition()
          });
         console.log(infowindow);
         google.maps.event.addListenerOnce(infowindow, 'closeclick', function() {
             markers[this.id].setVisible(true);
          });
          this.setVisible(false);
          infowindow.open(map);
       } );



    }
     function showDescription (){


     }


      //var infoWindow = new google.maps.InfoWindow({
      //   map: map
      //});

      //var contentString = '<div id="content">' +
      //   '<div id="siteNotice">' +
      //   '</div>' +
      //   '<h1 id="firstHeading" class="firstHeading">Uluru</h1>' +
      //   '<div id="bodyContent">' +
      //   '<p><b>Uluru</b>, also referred to as <b>Ayers Rock</b>, is a large ' +
      //   'sandstone rock formation in the southern part of the ' +
      //   'Northern Territory, central Australia. It lies 335&#160;km (208&#160;mi) ' +
      //   'south west of the nearest large town, Alice Springs; 450&#160;km ' +
      //   '(280&#160;mi) by road. Kata Tjuta and Uluru are the two major ' +
      //   'features of the Uluru - Kata Tjuta National Park. Uluru is ' +
      //   'sacred to the Pitjantjatjara and Yankunytjatjara, the ' +
      //   'Aboriginal people of the area. It has many springs, waterholes, ' +
      //   'rock caves and ancient paintings. Uluru is listed as a World ' +
      //   'Heritage Site.</p>' +
      //   '<p>Attribution: Uluru, <a href="https://en.wikipedia.org/w/index.php?title=Uluru&oldid=297882194">' +
      //   'https://en.wikipedia.org/w/index.php?title=Uluru</a> ' +
      //   '(last visited June 22, 2009).</p>' +
      //   '</div>' +
      //   '</div>';
      //
      //var infowindow = new google.maps.InfoWindow({
      //   content: contentString
      //
      //});
      //
      //var marker = new google.maps.Marker({
      //   position: uk,
      //   map: map,
      //   title: 'Uk (Ayers Rock)'
      //});
      //marker.addListener('click', function() {
      //   infowindow.open(map, marker);
      //   //priceBox.addClass('red');
     // //});
     //infowindow.open(map, marker);

     //  priceBox.on('click', function() {
    //     infowindow.open(map, marker);
    //     //priceBox.removeClass('red');
    //  })
 }
 // google.maps.event.addDomListener(window, 'load', init);
initMap();